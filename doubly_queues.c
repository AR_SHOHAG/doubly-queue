#include <stdio.h>
#define MAX 50

int queue_array[MAX];
int rear = -1;
int front = -1;
void main()
{
    int choice;
    while (1)
    {
        printf("1.Insert element to queue from rear \n");
        printf("2.Insert element to queue from front\n");
        printf("3.Delete element from queue from rear\n");
        printf("4.Delete element from queue from front\n");
        printf("5.Display all elements of queue \n");
        printf("6.Quit \n");
        printf("Enter your choice : ");
        scanf("%d", &choice);
        switch (choice) {

        case 1:
            insert_rear();
            break;
        case 2:
            insert_front();
            break;
        case 3:
            delete_rear();
            break;
        case 4:
            delete_front();
            break;
        case 5:
            display();
            break;
        case 6:
            exit(1);
        default:
            printf("Wrong choice \n");
        } /*End of switch*/
    } /*End of while*/
} /*End of main*/

void insert_rear()
{
    int add_item;
    if (rear == MAX -1)
        printf("Queue Overflow \n");
    else {
        if (front == -1){ /*If queue is initially empty */
            front = 0;
        }
            printf("Inset the element in queue : ");
            scanf("%d", &add_item);
            rear++;
            queue_array[rear] = add_item;
        }
}

void insert_front()
{
    int add_item;
    if (front<=0)
        printf("Failed \n");
    else {
            printf("Inset the element in queue : ");
            scanf("%d", &add_item);
            front--;
            queue_array[front] = add_item;
        }
}


void delete_front()
{
    if (front == -1 || front > rear)
    {
        printf("Queue Underflow \n");
        return ;
    }
    else {
        printf("Element deleted from queue is : %d\n", queue_array[front]);
        front = front + 1;
        }
}

void delete_rear()
{
    if (front == -1 || rear == -1)
    {
        printf("Failed\n");
        return ;
    }
    else {
        printf("Element deleted from queue is : %d\n", queue_array[rear]);
        rear--;
        }
}

display()
{
    int i;
    if (front == -1)
        printf("Queue is empty \n");
    else {
        printf("Queue is : \n");
        for (i= front; i<= rear; i++)
            printf("%d ", queue_array[i]);
            printf("\n");
        }
}
